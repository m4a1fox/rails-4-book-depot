#NOTES FROM THE BOOK
--------------------

## Chapter 6 notes:
-------------------

### A task

#### A1

`rails new depot` - rails will create new app and folder

## Chapter 5 notes:
-------------------

`rake db:migrate:redo` reverse and repeat uses last migration

## Chapter 4 notes:
-------------------

Ruby is OOP language

If we have class with name `LineItem` then we can create new object:
    
    line_item_one = LineItem.new
    line_item_one.quantity = 1
    
The Ruby will go to class `LineItem` find property quantity and increased the value
 
Ruby can run method without brackets. If you use the simple method, it is more likely the "command"

The local variable should start from lowercase.

    order
    line_item
    xr200

The variable of instance of the class start from symbol "at" (@). The variables and methods use snake_case.

    @quantity
    @product_id
    
The class name or constant should start from uppercase. The class name use CamelCase.
    
    Object
    PurchaseOrder
    LineItem
    
In Ruby also use the designations(обозначения) they used like keys in "array"

    redirect_to :action => "edit", :id => params[:id]
    :action
    :line
    :id
    
Designation is string literal, magic way transform into constant. :)    

The main point of using `:` in disignations name is like we've say: "Something with name id"

### Methods
-------------------

Create the method.
    
Go to console and run the `rails console`

    $ rails c(onsole)
    
And write the function:

    def say_goodnight(name)
        result = "Good night, " + name
        return result
    end
    
    puts say_goodnight("Max")
    puts say_goodnight("Jack")
    
To display smth in console, use `puts [string]` command. Also puts return `\n` symbol.

In Ruby comments in code use `#` symbol.

### Data Types 
--------------

#### Strings

If we used the single quotes `'` Ruby do nothing with the string.

If we used the double quoted `"` Ruby try to find substitution (подстановки) the char with `\` symbol. 

Also in double quotes we can use expression `#{expression}` replaced with expression value.

    def say_goodnight(name)
        result = "Good night, #{name}!"
        return result
    end

#### Array and hash

Array and hash in Ruby is index collection.

    a =[1, 'cat', 3,14]

`nil` is the object to show missing value.

With array also we can use method `<<()`. This method add value to its getter.

    ages = []
    for person in @people
        ages << person.age
    end
    -------
    a = []
    a << "Go!"
    puts a
    
For create array in Ruby use the example:
    
    a = ['Hello', 'world']
    # the same is
    a = %w{ Hello world }
    a # output: ["Hello", "world"]
    a[1] # output: "world"

Hash in Ruby is the same like array, with one different. The hash literal instead `[]` use `{}`. The hash in Ruby it is like `associative array` in `PHP`, but the key use `:` symbol on the key word beginning.

    car_properties = {
        :car => 'Renault',
        :color => 'White',
        :doors => 5,
        :year => 2011
    }
    
    puts car_properties # output: {:car=>"Renault", :color=>"White", :doors=>5, :year=>2011}
    puts car_properties[:year] # output: 2011
    
The left side (key) in Ruby we can use the any type of object.

Since Ruby 1.9 we can use another syntax:

    car_properties = {
        :car    'Renault',
        :color  'White',
        :doors  5,
        :year   2011
    }

We can send hash like arguments in methods.

Ruby allow now write brackets for hash if in arguments list hash is the last one.

    redirect_to :action => 'show', :id => product.id
    
#### RegExp

RegExp Ruby syntax template: `/pattern/` or `%r{pattern}`

    /Perl|Python/ # pattern with will find the word Perl or Python in the string
    
In patter above we use `|` it mean or the left word or the right word.
 
We can use brackets in pattern. So the patter `/Perl|Python/` we can rewrite like this one:
    
    /P(erl|ython)/
    
In programs to compare the string with RegExp we can use operator `=~`.

    if line =~ /P(erl|ython)/
        puts "Looks like here, used another programmer language "
    end
    
It patterns we can use repeated. Expression `/ab+c/` is the string which contain symbol `a`, 
after it go one or more `b` symbols after this symbol it's should be symbol `c`.
  
Special RegExp combinations:

    \d - one number
    \s - any space symbol
    \w - all alphabets symbols
    
### Logic
-------------------

Ruby have `if`, `while` loop

#### if else

    if count > 10
        puts "Try once more"
    elsif tries == 3
        puts "You loose"
    else
        puts "Insert the number"
    end

#### while
    
    while weight < 100 and num_pallets <= 30
    pallet = next_pallet()
    weight += pallet.weight
    num_pallets += 1
    end
    
Ruby also has instruction: `unless`. It's like `if`, but `unless` uses for check the on the unreliability (на недостоверность)
    
    x=1
    unless x>2
       puts "x is less than 2"
     else
      puts "x is greater than 2"
    end
    
`Until`
    
    $i = 0
    $num = 5
    
    until $i > $num  do
       puts("Inside the loop i = #$i" )
       $i +=1;
    end
    
Also we can use `modification instruction`

    puts "You loose" if point < 10
    puts "You can run now" while distance < 10
    
#### Blocks and iterations

Code Blocks - it's fragment of program, which placed between `{}` or inside the `do...end` words.
  
    {puts "Hello"} # this is block
   
    do                       ###
        club.enroll(person)  # and this is block too
        person.socialize     #
    end                      ###

To send block to method we should place the block in the end of the method. 
Another word we should place the start of the block in the end of the method

    greet { puts "Hello" }
    
If the calling of some method have arguments, they are writing before block

    verbose_greet("Dave", "permanent client") { puts "Hello" }

Method can run the connected with him block several times. The `yield` instruction it is something like we call method, 
which is call block, which connected with this method. Inside the block the arguments, getting the value, placed between vertical line `\`.

    animals = %w{cat dog fish bear}
    animals.each {|animal| puts animal + "!!!"}
    
Every `N` call method `times()`, which in this case call the connected with him method `N` times.

    3.times {print "Hey!"} # outoput: Hey! Hey! Hey!
    
Prefix operator `&` allow method to get the block like named argument.

    def wrap &b
        print "Santa say: "
        3.times(&b)
        print "\n"
    end
    
    wrap {print "Hey!"}
    
    
#### Exceptions

Exceptions is objects (class Exceptions and its sub-class).

To get exception we use method `raise`. So we break the normal behaviour of the code, and Ruby check the code for reverse stack for 
 
    begin
        content = load_blog_data(file_name)
    rescue BlogDataNotFound
        STDERR.puts "Файл #{file_name} не найден"
    rescue BlogDataFormatError
        STDERR.puts "В файле #{file_name} отсутствуют данные блога"
    rescue Exception => exc
        STDERR.puts " Общая ошибка загрузки #{file_name}: #{exc.message}"
    end

### Ruby organization structure

For organization methods in Ruby we have 2 main concepts (понятия): `classes` and `modules`

#### Class:

    class Order < ActiveRecord::Base
        has_many :line_items
        def self.find_all_unpaid
            self.where('paid = 0')
        end
        def total
            sum = 0
            line_items.each {|li| sum += li.total}
            sum
        end
    end
    
The class `Order` is sub-class `Base` class which is in `ActiveRecord` module.
`has_many` is the method form ActiveRecord.

Inside the class body we can define methods of the class. Add `self` prefix (3 line) we make this method class method, 
so it can be run in whole class. In our case we can run this method from any place of the program.

    to_collect = Order.find_all_unpaid
    
Class object save the state in variable instance. The name should be start from `@` symbol. To get this variables outside the class
is impossible. Instead of this should be created the methods which should return the value.

    class Greeter
        def initialize(name)
            @name = name
        end
        
        def name
            @name
        end
        
        def name=(new_name)
            @name = new_name
        end
    end
    
    g = Greeter.new("Barny")
    g.name # output: Barny
    g.name = "Betty"
    g.name # output: Betty
    
For creating all this methods Ruby provide the own (comfortable) methods.

    class Greeter
        attr_accessor   :name       # create method for read and write properties
        attr_reader     :greeting   # create only read method
        attr_write      :age        # create only write method

By default all method is open in Ruby class. But we can create private or protected methods.
 
    class MyClass
        def m1  # public / open method
        end
        
        protected # security method
        
        def m2
        end
        
        private
        
        def m3 # close method
        end
        
    end

#### Modules:

Modules it is almost class, but they get the collections of methods and constants.

If class mixed with some module, the module methods became available in class, like this method are written in the class.

We can mix with one module several class, and one class can be mixed with several modules.

    module StoreHelper
        def capitalize_words(string)
            string.split(' ').map {|word| word.capitalize}.join(' ')
        end
    end
    
#### YAML

`YAML` is recursive acronym, meaning YAML is not Markup Language.

In Rails we use `YAML` to config the DB settings
    
    development:
        adapter: sqlite3
        database: db/development.sqlite3
        pool: 5
        timeout: 5000
        
#### Маршализированные объекты

Ruby can convert the object it byte code and save it in file outside the program. Is is calling маршализацией. Then 
the copy of the object can be readed by other application or other program.

### Ruby idioms

Methods like: 

`empty! (method modifier)` and `empty? (method predicate)`. 
`Method modifier` make changes to recipient (получатель).
`Method predicate` return `true` or `false`.

`a || b` - is `or`
 
`a||=b` - is the same like a = a `operator` b

    count += 1          # same like: count = count + 1
    prise *= discount   # same like: prise = prise * discount
    count ||= 0         # same like: count = count || 0, so if count have not values it will be 0
     
`require File.expand_path('../../config/environment', __FILE__)` - allow insert file from an external (внешнего) source
    

## Chapter 3 notes:
-------------------

HTTP common methods:
    
    POST
    GET
    PUT
    PATCH
    DELETE
    
In common case, when we send `POST` query type we run `create()` method in controller

## Chapter 2 notes:
-------------------

Create rails new project:
    
    $ cd work_folder
    $ rails new project_name
    $ cd project_name
    

Run rails server

    $ rails s(erver) [:optional -p pornNumber]
    
Then go to:
    
    http://localhost:[portNumber]
    
Default post is `8000`

    
Generate controller ControllerName with path Route1 and Route2

    $ rails g(enerate) controller ControllerName Route1 Route2
    
Generate link:
In template write:
    
    <%= link_to "link word", path %>
    
To get path in console write:
    
    $ rake routes
    
get Preffix + `_path`

Concatenation in ruby - `+` symbol

Dump in `erb` files:
    
    <%= debug([object]) %>
    
Get all files in dir to variable:
    
    @files = Dir.glob(folder)
    
Read the loop (object)

    <% for item in loop %>
        <%= item %>
    <% end %>
    
## Chapter 1 notes:
-------------------

Get all rails version on the computer
    
    $ gem list --local rails
    
Read file in command line

    $ tail -f [file path]

Default `Rails` database is `SQLite3`

Rails work with such database: `SQLite3`, `DB2`, `MySQL`, `Oracle`, `Postgres`, `Firebird`, `SQL Server`

Create Rails own documentation

    $ rails new dummy_app
    $ cd dummy_app
    $ rake doc:rails